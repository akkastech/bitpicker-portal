<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('avatar')->default('default.jpg');
            $table->enum('status', array(0, 1))->default(1);
            $table->string('email', 30)->unique();
            $table->string('password');
            $table->integer('user_role_idFk')->unsigned();
            $table->timestamps();
            $table->rememberToken();
        });
        Schema::table('users',function ($table){
//            $table->foreign('order_idFk')->references('order_id')->on('orders')->onDelete('cascade');
            $table->foreign('user_role_idFk')->references('role_id')->on('user_roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
