<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->string('phone')->nullable();
            $table->integer('amount_btc')->unsigned()->nullable();
            $table->string('btc_wallet')->nullable();
            $table->string('document')->nullable();
//            $table->enum('status', array('Accepted','Rejected'.'Pending','Completed'))->default('Pending');
            $table->enum('status', array('Accepted', 'Rejected','Pending','Completed','Nothing'))->default('Nothing');
            $table->integer('user_idFk')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('orders', function($table) {
            $table->foreign('user_idFk')->references('id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
