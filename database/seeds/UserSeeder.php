<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Super Admin',
                'email'     => 'admin@admin.com',
                'password'  => bcrypt('admin'),
                'user_role_idFk' => 1,
            ],

            [
                'name' => 'Ramisha',
                'email' => 'misha@gmail.com',
                'password' => bcrypt('misha'),
                'user_role_idFk' => 2,
            ],
            [
                'name' => 'Jia',
                'email' => 'jia@gmail.com',
                'password' => bcrypt('jia'),
                'user_role_idFk' => 2,
            ],
            [
                'name' => 'User',
                'email' => 'user@user.com',
                'password' => bcrypt('user'),
                'user_role_idFk' => 2,
            ]
        ]);
        DB::table('orders')->insert([
            [
                'phone' => 2435345344,
                'amount_btc'     => 546,
                'btc_wallet'  => 5084,
                'user_idFk' => 2,
                'created_at' => '2017-10-16 10:29:36'
            ],

            [
                'phone' => 2435345344,
                'amount_btc'     => 5545,
                'btc_wallet'  => 3447,
                'user_idFk' => 2,
                'created_at' => '2017-10-16 10:29:36'
            ],
            [
                'phone' => 2435345344,
                'amount_btc'     => 45356,
                'btc_wallet'  => 5237,
                'user_idFk' => 3,
                'created_at' => '2017-10-16 10:29:36'
            ],
            [
                'phone' => 2435345344,
                'amount_btc'     => 546,
                'btc_wallet'  => 56757,
                'user_idFk' => 3,
                'created_at' => '2017-10-16 10:29:36'
            ],
            [
                'phone' => 2435345344,
                'amount_btc'     => 5126,
                'btc_wallet'  => 43536,
                'user_idFk' => 4,
                'created_at' => '2017-10-16 10:29:36'
            ],
            [
                'phone' => 2435345344,
                'amount_btc'     => 546,
                'btc_wallet'  => 56757,
                'user_idFk' => 4,
                'created_at' => '2017-10-16 10:29:36'
            ]
        ]);
    }
}
