<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankInfo extends Model
{
    protected $primaryKey = 'account_id';
    protected $table = 'bank_infos';
}
