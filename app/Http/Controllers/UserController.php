<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    public function dashboard()
    {
//        dd($user_id = Auth::id());

//         $user = User::findorfail(Auth::id());
        $user = User::find(Auth::id());
        $userorders = $user->orders()->get();
//        dd($userorders);
        return view('user.dashboard',compact('userorders'));
    }
    public function profile_update(Request $request,$id)
    {
        $admin = User::findorfail($id);
        $admin->email = $request->email;
        $admin->name = $request->name;
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('files/images/' . $filename ) );
            $admin = Auth::user();
            $admin->avatar = $filename;
        }
        $admin->password = bcrypt($request->password);
        $admin->save();
        return redirect()->back()->with('success','Your Profile is updated successfully');
    }

    public function change_password(Request $request,$id)
    {
        $old_password = Auth::user()->password;
        $user = User::findorfail($id);

        if (Hash::check($request->current_password, $old_password))
        {
            if ($request->password == $request->password_confirmation)
            {
                $user->password = bcrypt($request->password);
                $user->save();
                return redirect()->back()->with('success', 'Your Password is changed successfully');
            }
            else
            {
                return redirect()->back()->with('error', 'Re-Enter Password is not correct');
            }
        }
        else
        {

            return redirect()->back()->with('error', 'Old password donot match please try again');
        }
    }
}
