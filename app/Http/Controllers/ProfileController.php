<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function user_profile()
    {
        return view('user.profile.user_profile');
    }
    public function profile_edit()
    {
        return view('user.profile.profile_edit');
    }
    
}
