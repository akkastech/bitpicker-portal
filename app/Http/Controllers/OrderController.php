<?php

namespace App\Http\Controllers;

use App\BankInfo;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class OrderController extends Controller
{
    public function order_form()
    {
        return view('user.order.order_form');
    }
    public function order_save(Request $request)
    {
            $order = new Order();
            $order->phone = $request->phone;
            $order->amount_btc =$request->amount_btc;
            $order->btc_wallet  = $request->btc_wallet;
            $order->user_idFk  = Auth::id();
//        $filename = $request->file('document')->getClientOriginalName();
//        $request->file('document')->move('files/documents/', $filename);
//        $order->document = $filename;
            $order->created_at  = Carbon::now();
            $order->save();
            return redirect('user/dashboard')->with('success','Your order is saved successfully');

    }
    public function document_save(Request $request)
    {

        $order = Order::findorfail($request->order_id);
        $filename = $request->file('document')->getClientOriginalName();
        $request->file('document')->move('files/documents/', $filename);
        $order->document = $filename;
        $order->status = 'Pending';
        $order->save();
        
        return redirect('user/dashboard')->with('success','Receipt is uploaded successfully');
    }

    
    public function bank_info()
    {

        /*
         $quote = false;
        if(Quote::count() > 0){
            $quote = Quote::findOrFail(1);
        }
        return view('admin.quote.quote_add', compact('quote'));
         */






        $bank_info = false;
        if (BankInfo::count() > 0)
        {
            $bank_info = BankInfo::findorfail(1);    
        }
        
        return view('user.order.bank_info',compact('bank_info'));
    }
}
