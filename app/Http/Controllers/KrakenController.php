<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Curl;

class KrakenController extends Controller
{
	protected $key;
	protected $secret;

    public function __construct()
    {
    	$this->key = 'P8w83/5dwr9axtMeVNXmiEe7Dhb8uZzTWb9L1OSuUDd06G1qQHA/uXPx';
    	$this->secret = '32OKgjWi1NtrHoflbfEUGVb2rP4iV0XvN2y/Y1kldai64iE4BGfZ8pibwlj5f1TyvYUSVFVsMDLFwaLWTfvAjA==';
    }

    public function all_calls()
    {
        $data = [];
        $response = Curl::to('https://api.kraken.com/0/public/Ticker?pair=XXBTZUSD')
                            ->asJson()
                            ->get();
        if(empty($response->error)){
            $usd = $response->result;
            $ask = '';
            $color = '';
            $avrage = '';
            if(count($usd) > 0){
                foreach ($usd as $key => $value) {
                    $ask = $value->a[0];
                    $avrage = substr($value->l[0]/$value->h[0], 0, 3);
                    if (strpos($avrage, '-') !== false) {
                        $color = 'red';
                    }else{
                        $color = 'green';
                    }
                }
            }

            $data['kraken']['ask']    = explode('.', $ask)[0];
            $data['kraken']['point']  = substr(explode('.', $ask)[1], 0, 2);
            $data['kraken']['color']  = $color;
            $data['kraken']['avrage'] = $avrage.' %';
            
        }else{
            $data['kraken']['ask']    = '0000';
            $data['kraken']['point']  = '00';
            $data['kraken']['color']  = 'green';
            $data['kraken']['avrage'] = 'N/A';
        }

        $response = Curl::to('https://api.bitfinex.com/v1/pubticker/btcusd')
                            ->asJson()
                            ->get();
        
        if(isset($response->ask)){
            $ask = $response->ask;
            $color = '';
            $avrage = substr($response->low/$response->high, 0, 3);
            
             if (strpos($avrage, '-') !== false) {
                $color = 'red';
            }else{
                $color = 'green';
            }
            $data['bitfinex']['ask']    = explode('.', $response->ask)[0];
            $data['bitfinex']['point']  = explode('.', $response->ask)[1];
            $data['bitfinex']['color']  = $color;
            $data['bitfinex']['avrage'] = $avrage.' %';
            
        }else{
            $data['bitfinex']['ask']    = '0000';
            $data['bitfinex']['point']  = '00';
            $data['bitfinex']['color']  = 'green';
            $data['bitfinex']['avrage'] = 'N/A';
        }

        $response = Curl::to('https://bittrex.com/api/v1.1/public/getmarketsummary?market=usdt-btc')
                            ->asJson()
                            ->get();
        
        if(isset($response->success)){
            $ask = '';
            $color = '';
            $avrage = '';
            if(count($response->result) > 0){
                foreach ($response->result as $key => $value) {
                    $ask = $value->Ask;
                    $avrage = substr($value->Low/$value->High, 0, 3);
                    if (strpos($avrage, '-') !== false) {
                        $color = 'red';
                    }else{
                        $color = 'green';
                    }
                }
            }

            if(count(explode('.', $ask)) > 1){
                $data['bittrex']['ask']    = explode('.', $ask)[0];
                $data['bittrex']['point']  = substr(explode('.', $ask)[1], 0, 2);
            }else{
                $data['bittrex']['ask']    = $ask;
                $data['bittrex']['point']  = '00';
            }
            $data['bittrex']['color']  = $color;
            $data['bittrex']['avrage'] = $avrage.' %';
            
        }else{
            $data['bittrex']['ask']    = '0000';
            $data['bittrex']['point']  = '00';
            $data['bittrex']['color']  = 'green';
            $data['bittrex']['avrage'] = 'N/A';
        }

        $response = Curl::to('http://api.hitbtc.com/api/1/public/BTCUSD/ticker')
                            ->asJson()
                            ->get();
        
        if(isset($response->ask)){
            $ask = $response->ask;
            $color = '';
            $avrage = substr($response->low/$response->high, 0, 3);
            
             if (strpos($avrage, '-') !== false) {
                $color = 'red';
            }else{
                $color = 'green';
            }
            $data['hitbtc']['ask']    = explode('.', $response->ask)[0];
            $data['hitbtc']['point']  = explode('.', $response->ask)[1];
            $data['hitbtc']['color']  = $color;
            $data['hitbtc']['avrage'] = $avrage.' %';
            
        }else{
            $data['hitbtc']['ask']    = '0000';
            $data['hitbtc']['point']  = '00';
            $data['hitbtc']['color']  = 'green';
            $data['hitbtc']['avrage'] = 'N/A';
        }

        $response = Curl::to('https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC&depth=1')
                            ->asJson()
                            ->get();
        
        if(isset($response)){
            if(count($response->asks) > 0){
                $ask = $response->asks[0][0];

                $data['poloniex']['ask']    = explode('.', $ask)[0];
                $data['poloniex']['point']  = substr(explode('.', $ask)[1], 0, 2);
                
            }else{
                $data['poloniex']['ask']    = '0000';
                $data['poloniex']['point']  = '00';
            }
        }else{
            $data['poloniex']['ask']    = '0000';
            $data['poloniex']['point']  = '00';
        }

        

        return $data;
    }

    // public function get_bitfinex()
    // {
        
    //     $response = Curl::to('https://api.bitfinex.com/v1/pubticker/btcusd')
    //                         ->asJson()
    //                         ->get();
        
    //     if($response->ask){
    //         $ask = $response->ask;
    //         $color = '';
    //         $avrage = substr($response->low/$response->high, 0, 3);
            
    //          if (strpos($avrage, '-') !== false) {
    //             $color = 'red';
    //         }else{
    //             $color = 'green';
    //         }
    //         $data['ask']    = explode('.', $response->ask)[0];
    //         $data['point']  = explode('.', $response->ask)[1];
    //         $data['color']  = $color;
    //         $data['avrage'] = $avrage.' %';
            
    //         return $data;
    //     }
    // }

    // public function get_bittrex()
    // {
        
    //     $response = Curl::to('https://bittrex.com/api/v1.1/public/getmarketsummary?market=usdt-btc')
    //                         ->asJson()
    //                         ->get();
        
    //     if($response->success){
    //         $ask = '';
    //         $color = '';
    //         $avrage = '';
    //         if(count($response->result) > 0){
    //             foreach ($response->result as $key => $value) {
    //                 $ask = $value->Ask;
    //                 $avrage = substr($value->Low/$value->High, 0, 3);
    //                 if (strpos($avrage, '-') !== false) {
    //                     $color = 'red';
    //                 }else{
    //                     $color = 'green';
    //                 }
    //             }
    //         }

    //         if(count(explode('.', $ask)) > 1){
    //             $data['ask']    = explode('.', $ask)[0];
    //             $data['point']  = substr(explode('.', $ask)[1], 0, 2);
    //         }else{
    //             $data['ask']    = $ask;
    //             $data['point']  = '00';
    //         }
    //         $data['color']  = $color;
    //         $data['avrage'] = $avrage.' %';
            
    //         return $data;
    //     }
    // }
    
    // public function get_hitbtc()
    // {
        
    //     $response = Curl::to('https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC&depth=1')
    //                         ->asJson()
    //                         ->get();
        
    //     if(count($response->asks) > 0){
    //         $ask = $response->asks[0][0];

    //         $data['hitbtc']['ask']    = explode('.', $ask)[0];
    //         $data['hitbtc']['point']  = substr(explode('.', $ask)[1], 0, 2);
    //         dd($data);
    //     }
    // }

    

    


}
