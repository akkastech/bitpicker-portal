<?php

namespace App\Http\Controllers;

use App\BankInfo;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Image;

//use Intervention\Image\Image;
//
class AdminController extends Controller
{
    public function dashboard()
    {
        $orders = Order::all()->count();
        $order_pending = Order::where('status','=','Pending')->count();
        $order_completed = Order::where('status','=','Completed')->count();
        return view('admin.dashboard',compact('orders','order_pending','order_completed'));
    }
    public function order_list()
    {
        $orders = Order::all();
        return view('admin.order_list',compact('orders'));
    }
//    public function user_detail($id)
//    {
//        $order = Order::findorfail($id);
//        return view('admin.user_detail',compact('order'));
//    }
    public function profile_edit()
    {
        if(Auth::User()->user_role_idFk == 1){
            return view('admin.profile_edit');
        }else if(Auth::User()->user_role_idFk == 2){
            return view('user.profile_edit');
        }
    }
    public function profile_update(Request $request,$id)
    {
        $admin = User::findorfail($id);
        $admin->email = $request->email;
        $admin->name = $request->name;

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('files/images/' . $filename ) );
            $admin = Auth::user();
            $admin->avatar = $filename;
        }

        $admin->save();
        return redirect()->back()->with('success','Your Profile is updated successfully');
    }

    public function change_password(Request $request,$id)
    {
        $old_password = Auth::user()->password;
        $user = User::findorfail($id);

        if (Hash::check($request->current_password, $old_password))
        {
            if ($request->password == $request->password_confirmation)
            {
                $user->password = bcrypt($request->password);
                $user->save();
                return redirect()->back()->with('success', 'Your Password is changed successfully');
            }
            else
            {

                return redirect()->back()->with('error', 'Re-Enter Password is not correct');
            }
        }
        else
        {
            return redirect()->back()->with('error', 'Old password do not match please try again');
        }
    }

    public function user_list()
    {
        $users = User::where('user_role_idFk','!=' ,1)->get();
        return view('admin.user_list',compact('users'));
    }
    
    public function order_detail($id)
    {
        $user = User::findorfail($id);
//        dd($user->orders);
        return view('admin.order_detail',compact('user'));
        
    }
    public function user_status($id)
    {
        $user = User::findorfail($id);
        if ($user->status == '0')
        {
            $user->status = '1';
            $user->save();
        }elseif ($user->status == '1')
        {
            $user->status = '0';
            $user->save();
        }
        return redirect('admin/user_list');
    }
    public function status_accepted($id)
    {
        $order = Order::findorfail($id);
        $order->status = 'Accepted';
        $order->save();

        $view = 'user.mail';
        $user = User::findOrFail($order->user_idFk);
        $data['user'] = $user;
        $data['order'] = $order;
        $data['Mailmessage'] = '<strong>Congratulations:</strong>'.' You\'r Order has been accepted.';
        $subject = 'Accepted';
        \Mail::send($view, $data, function ($message) use($user, $subject){
            $message->to($user->email)->subject($subject);
        });

        return redirect()->back()->with('success','Order is accepted successfully');
    }
    public function status_completed($id)
    {
        $order = Order::findorfail($id);
        $order->status = 'Completed';
        $order->save();

        $view = 'user.mail';
        $user = User::findOrFail($order->user_idFk);
        $data['user'] = $user;
        $data['Mailmessage'] = '<strong>Congratulations:</strong>'.' You\'r Order has been Completed.';
        $subject = 'Completed';
        \Mail::send($view, $data, function ($message) use($user, $subject){
            $message->to($user->email)->subject($subject);
        });
        return redirect()->back()->with('success','Order is completed successfully');
    }
    public function status_rejected($id)
    {
        $order = Order::findorfail($id);
        $order->status = 'Rejected';
        $order->save();

        $view = 'user.mail';
        $user = User::findOrFail($order->user_idFk);
        $data['user'] = $user;
        $data['Mailmessage'] = '<strong>Warring:</strong>'.' You\'r Order has been rejected.';
        $subject = 'Rejected';
        \Mail::send($view, $data, function ($message) use($user, $subject){
            $message->to($user->email)->subject($subject);
        });
        return redirect()->back()->with('success','Status is Rejected successfully');
    }
    public function bank_acc()
    {
        $bankinfo = false;
        if(BankInfo::count() > 0){
            $bankinfo = BankInfo::findOrFail(1);
        }
        return view('admin.bankaccount_info',compact('bankinfo'));
    }
    
    public function save_accountinfo(Request $request)
    {
        if (BankInfo::count() > 0)
        {
            $bankinfo = BankInfo::findorfail(1);
        }else
        {
            $bankinfo = new BankInfo();
        }
        $bankinfo->title = $request->title;
        $bankinfo->account_num = $request->account_num;
        $bankinfo->bank_name = $request->bank_name;
        $bankinfo->save();
        return redirect('admin/bank_acc')->with('success','Saved Successfully!');


    }
}
