<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey = 'order_id';
    protected $table = 'orders';

    public function user()
    {
        return $this->belongsTo('App\User','user_idFk','id');
    }
}
