<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middlewareGroups' => 'web'], function () {

    Route::get('/', 'FrontController@index');
    Route::get('search/companies', 'FrontController@search_result');
    Route::post('subscribe', 'FrontController@subscribe');
    Route::get('all/ajax/calls', 'KrakenController@all_calls');
    // Route::get('bitfinex', 'KrakenController@get_bitfinex');
    Route::get('hitbtc', 'KrakenController@get_hitbtc');

//    Route::get('login', 'FrontController@login_page');
    Route::get('register', 'Auth\RegisterController@register');
    Route::post('register_user', 'Auth\RegisterController@register_user');
    Route::get('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout');
    Route::post('authuser','Auth\LoginController@authenticateuser');
    Route::get('dashboard', 'FrontController@dashboard');
    Route::get('insert', 'FrontController@insert');
    Route::post('userauth', 'FrontController@login_submit');
//    Route::post('user/profile_update/{id}', 'UserController@profile_update');
//    Route::post('user/change_password/{id}', 'UserController@change_password');
    
});

Route::group(['middleware' => 'adminAuth'], function () {
    Route::get('admin/dashboard', 'AdminController@dashboard');
    Route::get('admin/user_list', 'AdminController@user_list');
    Route::get('admin/user_detail/{id}', 'AdminController@user_detail');
    Route::get('admin/order_list', 'AdminController@order_list');
    Route::get('admin/order_detail/{id}', 'AdminController@order_detail');
    Route::get('admin/user_status/{id}', 'AdminController@user_status');
    Route::get('admin/profile_edit', 'AdminController@profile_edit');
    Route::post('admin/profile_update/{id}', 'AdminController@profile_update');
    Route::post('admin/change_password/{id}', 'AdminController@change_password');
    Route::get('admin/status_accepted/{id}', 'AdminController@status_accepted');
    Route::get('admin/status_completed/{id}', 'AdminController@status_completed');
    Route::get('admin/status_rejected/{id}', 'AdminController@status_rejected');
    Route::get('admin/bank_acc', 'AdminController@bank_acc');
    Route::post('admin/save_accountinfo', 'AdminController@save_accountinfo');
});


Route::group(['middleware' => 'userAuth'], function () {
    Route::get('user/dashboard', 'UserController@dashboard');
    Route::get('user/order_form', 'OrderController@order_form');
    Route::get('user/bank_info', 'OrderController@bank_info');
//    Route::get('user/user_profile', 'ProfileController@user_profile');
    Route::post('user/order_save', 'OrderController@order_save');
    Route::post('user/document_save', 'OrderController@document_save');
    Route::get('user/profile_edit','ProfileController@profile_edit');
    Route::post('user/profile_update/{id}', 'UserController@profile_update');
    Route::post('user/change_password/{id}', 'UserController@change_password');
});