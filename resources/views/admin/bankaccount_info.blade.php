@extends('admin.layout')
@section('title','dashboard')
@section('content')
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-home"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Bank Account Information</li>
                    </ul>
                    <h4>Add Info</h4>
                </div>
            </div><!-- media -->
        </div><!-- page -->
        <div class="white-box">

                <div class="panel panel-primary" style="margin-bottom:0px;">
                    <div class="panel-heading">Add Account Info</div>
                </div>
            @if(session('success'))
                <p class="alert alert-success" style="background: white">{{session('success')}}</p>
            @endif
                <br>
                <form id="form-change-password" role="form" method="POST" action="{{ url('admin/save_accountinfo')}}"  class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="col-md-12">
                        <label for="current-password" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" value="@if($bankinfo) {{$bankinfo->title}} @endif" class="form-control" id="current-password" name="title" placeholder="Title" required="">
                            </div>
                        </div>
                        <label for="password" class="col-sm-2 control-label">Account Number</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" value="@if($bankinfo) {{$bankinfo->account_num}} @endif" class="form-control numbersOnly" name="account_num" placeholder="Account Number"  maxlength="20" required>
                            </div>
                        </div>
                        <label for="password_confirmation" class="col-sm-2 control-label">Bank Name</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" value="@if($bankinfo) {{$bankinfo->bank_name}} @endif" class="form-control" id="password_confirmation" name="bank_name" placeholder="Bank Number" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger text-uppercase pull-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>

@endsection
@section('foot')
    @parent
            <script>

                jQuery('.numbersOnly').keyup(function () {
                    this.value = this.value.replace(/[^0-9\.]/g,'');
                });
            </script>
@endsection