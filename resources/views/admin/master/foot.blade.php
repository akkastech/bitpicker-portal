<script src="{{url('files/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{url('files/js/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{url('files/js/bootstrap.min.js')}}"></script>
<script src="{{url('files/js/modernizr.min.js')}}"></script>
<script src="{{url('files/js/pace.min.js')}}"></script>
<script src="{{url('files/js/retina.min.js')}}"></script>
<script src="{{url('files/js/jquery.cookies.js')}}"></script>

<script src="{{url('files/js/flot/jquery.flot.min.js')}}"></script>
<script src="{{url('files/js/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{url('files/js/flot/jquery.flot.spline.min.js')}}"></script>
<script src="{{url('files/js/jquery.sparkline.min.js')}}"></script>
<script src="{{url('files/js/morris.min.js')}}"></script>
<script src="{{url('files/js/raphael-2.1.0.min.js')}}"></script>
<script src="{{url('files/js/bootstrap-wizard.min.js')}}"></script>
<script src="{{url('files/js/select2.min.js')}}"></script>

<script src="{{url('files/js/custom.js')}}"></script>
<script src="{{url('files/js/dashboard.js')}}"></script>