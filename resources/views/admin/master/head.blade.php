<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title>Chain Responsive Bootstrap3 Admin</title>

<link href="{{url('files/css/style.default.css')}}" rel="stylesheet">
<link href="{{url('files/css/morris.css')}}" rel="stylesheet">
<link href="{{url('files/css/select2.css')}}" rel="stylesheet" />
<link href="{{url('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{url('https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" />


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="{{url('files/js/html5shiv.js')}}"></script>
<script src="{{url('files/js/respond.min.js')}}"></script>
<![endif]-->