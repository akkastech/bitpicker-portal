@extends('admin.layout')
@section('title','dashboard')
@section('content')
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-th-list"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-list"></i></a></li>
                        <li><a href="">List</a></li>
                        <li>Orders List</li>
                    </ul>
                    <h4>Orders List</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->

        <div class="contentpanel">
            @if(session('success'))
                <p class="alert alert-success" style="background: white">{{session('success')}}</p>
            @endif
            <table id="example" class="table table-striped table-bordered responsive">
                <thead class="">
                <tr>
                    <th>Order Id</th>
                    <th>BTC Amount</th>
                    <th>Wallet Number</th>
                    <th>Phone No.</th>
                    <th>Document</th>
                    <th>Status</th>
                    {{--<th>Action</th>--}}
                </tr>
                </thead>

                <tbody>
                @if(count($user->orders)>0)
                    @foreach($user->orders as $order)
                    <tr>
                        <td>{{$order->order_id}}</td>
                        <td>{{$order->amount_btc}}</td>
                        <td>{{$order->btc_wallet}}</td>
                        <td>{{$order->phone}}</td>
                        <td>@if($order->document != NULL)<a href='{{url('files/documents/'.$order->document)}}' download="{{url('files/documents/'.$order->document)}}" class="btn btn-primary">Download</a> @else <span class="label label-danger">N/A</span> @endif</td>
                        <td>
                            @if($order->status == 'Nothing')
                                <span class="label label-default"> Upload Receipt</span>
                            @elseif($order->status == 'Pending')
                                <a href="{{url('admin/status_accepted/'.$order->order_id)}}" class="btn btn-success">Accept</a>&nbsp;&nbsp;&nbsp;<a  href="{{url('admin/status_rejected/'.$order->order_id)}}" class="btn btn-warning" role="button">Reject</a>
                                @elseif($order->status == 'Accepted')
                                <a href="{{url('admin/status_completed/'.$order->order_id)}}" class="btn btn-info" role="button">Complete</a>
                            @elseif($order->status == 'Rejected')
                                <span class="label label-danger">Rejected</span>
                            @elseif($order->status == 'Completed')
                                <span class="label label-success">completed</span>
                            @endif
                        </td>
                        {{--<td><span @if($order->status == 'Nothing') class="label label-default" @elseif($order->status == 'Rejected') class="label label-warning"   @elseif($order->status == 'Accepted') class="label label-info" @elseif($order->status == 'Completed') class="label label-success"@endif>{{$order->status}}</span></td>--}}
                        {{--<td>@if($order->status == 'Pending')<a href="{{url('admin/status_accepted/'.$order->order_id)}}" class="btn btn-success">Accepted</a>&nbsp;&nbsp;&nbsp;<a  class="btn btn-warning" role="button">Rejected</a>@elseif($order->status == 'Accepted') <a href="{{url('admin/status_completed/'.$order->order_id)}}" class="btn btn-success" role="button">Completed</a>
                        @elseif($order->status == 'Completed')<a class="btn btn-success disabled">Completed</a>@endif</td>--}}
                    </tr>
                @endforeach
                    @endif
                </tbody>
            </table>
        </div><!-- contentpanel -->
    </div><!-- mainpanel -->
@endsection
@section('foot')
    @parent
    <script src="{{url('//code.jquery.com/jquery-1.12.4.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

    </script>
@endsection