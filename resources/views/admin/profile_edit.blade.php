@extends('admin.layout')
@section('title','dashboard')
@section('content')
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-th-list"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-list"></i></a></li>
                        <li><a href="">List</a></li>
                        <li>User List</li>
                    </ul>
                    <h4>User List</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->

        <div class="contentpanel">
            @if(session('success'))
                <h5 class="alert alert-success"><b>{{session('success')}}</b></h5>
            @endif
            @if(session('error'))
                <div class="alert alert-danger">
                    <b>{{session('error')}}</b>
                </div>
            @endif
            <div class="white-box">
                <img src="{{url('files/images/'.Auth::user()->avatar)}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px; margin-bottom:10px;">
                <h2> {{Auth::user()->name }}'s Profile</h2>
                </br>
                <form class="form-horizontal form-material" id="loginform" action="{{url('admin/profile_update/'.\Illuminate\Support\Facades\Auth::user()->id)}}" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <label>Update Profile Image</label>
                    <input type="file" name="avatar">
                    <div class="col-md-12">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder=" User name" name="name" value="{{Auth::user()->name}}">
                            </div>
                        </div>
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input class="form-control" type="email" required="" placeholder="Email" name="email" value="{{Auth::user()->email}}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary text-uppercase pull-right">Update</button>
                        </div>
                    </div>
                </form>

                <div class="panel panel-primary" style="margin-bottom:0px;">
                    <div class="panel-heading">Change Password</div>
                </div>
                <br>
                <form id="form-change-password" role="form" method="POST" action="{{ url('admin/change_password/'.\Illuminate\Support\Facades\Auth::user()->id) }}"  class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="col-md-12">
                        <label for="current-password" class="col-sm-2 control-label">Current Password</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="password" value="" class="form-control" id="current-password" name="current_password" placeholder="Password" required="">
                            </div>
                        </div>
                        <label for="password" class="col-sm-2 control-label">New Password</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <label for="password_confirmation" class="col-sm-2 control-label">Re-enter Password</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger text-uppercase pull-right">Change</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- contentpanel -->
    </div><!-- mainpanel -->
@endsection
@section('foot')
    @parent
    <script src="{{url('//code.jquery.com/jquery-1.12.4.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection