<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    @include('admin.master.head')
</head>
<body>
@include('admin.master.header')
<!-- START CONTENT -->
<section>
<div class="mainwrapper">
    @include('admin.master.sidebar')
    @yield('content')
</div>
</section>
@section('foot')
    @include('admin.master.foot')
@show
<!-- End Content -->
</body>
</html>