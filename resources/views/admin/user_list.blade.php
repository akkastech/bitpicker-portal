@extends('admin.layout')
@section('title','dashboard')
@section('content')
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-th-list"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-list"></i></a></li>
                        <li><a href="">List</a></li>
                        <li>User List</li>
                    </ul>
                    <h4>User List</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->

        <div class="contentpanel">
            <table id="example" class="table table-striped table-bordered responsive">
                <thead class="">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Orders</th>
                    <th>Status</th>
                </tr>
                </thead>

                <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td><a href="{{url('admin/order_detail/'.$user->id)}}" class="btn btn-primary" role="button">Detail</a></td>
                    <td><a href="{{url('admin/user_status/'.$user->id)}}" class="btn btn-warning" role="button"> @if($user->status == 0) Active @elseif($user->status== 1) Deactive @endif</a></td>
                </tr>
               @endforeach
                </tbody>
            </table>
        </div><!-- contentpanel -->
    </div><!-- mainpanel -->
@endsection
@section('foot')
    @parent
   <script src="{{url('//code.jquery.com/jquery-1.12.4.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

    </script>
@endsection