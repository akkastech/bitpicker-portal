@extends('admin.layout')
@section('title','dashboard')
@section('content')
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-th-list"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-list"></i></a></li>
                        <li><a href="">List</a></li>
                        <li>Order List</li>
                    </ul>
                    <h4>Order List</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->

        <div class="contentpanel">
            <table id="example" class="table table-striped table-bordered responsive">
                <thead class="">
                <tr>
                    <th>Order Id</th>
                    <th>BTC Amount</th>
                    <th>Wallet Number</th>
                    <th>Phone No.</th>
                    <th>Document</th>
                    <th>User Name</th>
                    {{--<th>Status</th>--}}
                </tr>
                </thead>

                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->order_id}}</td>
                        <td>{{$order->amount_btc}}</td>
                        <td>{{$order->btc_wallet}}</td>
                        <td>{{$order->phone}}</td>
                        <td>@if($order->document != NULL)<a href='{{url('files/documents/'.$order->document)}}' download="{{url('files/documents/'.$order->document)}}" class="btn btn-primary">Download</a> @else <span class="label label-danger">N/A</span> @endif</td>
                        <td>
                            {{--<a href="{{url('admin/user_detail/'.$order->order_id)}}" class="btn btn-primary" role="button"></a>--}}
                            <a href="{{url('admin/order_detail/'.$order->user->id)}}" role="button">{{$order->user->name}}</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- contentpanel -->
    </div><!-- mainpanel -->
@endsection
@section('foot')
    @parent
    <script src="{{url('//code.jquery.com/jquery-1.12.4.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

    </script>
@endsection