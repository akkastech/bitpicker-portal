@extends('admin.layout')
@section('title','dashboard')
@section('content')
    <div class="mainpanel">
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-home"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                        <li>Dashboard</li>
                    </ul>
                    <h4>Dashboard</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->

        <div class="contentpanel">

            <div class="row row-stat">
                <div class="col-md-4">
                    <div class="panel panel-success-alt noborder">
                        <div class="panel-heading noborder">
                            <div class="panel-btns">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                            </div><!-- panel-btns -->
                            <div class="panel-icon"><i class="fa fa-list"></i></div>
                            <div class="media-body">
                                <h5 class="md-title nomargin">Orders</h5>
                                <h3>Total Orders</h3>
                                <h1 class="mt5">{{$orders}}</h1>
                            </div><!-- media-body -->
                            <hr>
                            {{--<div class="clearfix mt20">--}}
                                {{--<div class="pull-left">--}}
                                    {{--<h5 class="md-title nomargin">Yesterday</h5>--}}
                                    {{--<h4 class="nomargin">$29,009.17</h4>--}}
                                {{--</div>--}}
                                {{--<div class="pull-right">--}}
                                    {{--<h5 class="md-title nomargin">This Week</h5>--}}
                                    {{--<h4 class="nomargin">$99,103.67</h4>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        </div><!-- panel-body -->
                    </div><!-- panel -->
                </div><!-- col-md-4 -->

                <div class="col-md-4">
                    <div class="panel panel-primary noborder">
                        <div class="panel-heading noborder">
                            <div class="panel-btns">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                            </div><!-- panel-btns -->
                            <div class="panel-icon"><i class="fa fa-users"></i></div>
                            <div class="media-body">
                                <h5 class="md-title nomargin">Orders</h5>
                                <h3>Pending Orders</h3>
                                <h1 class="mt5">{{$order_pending}}</h1>
                            </div><!-- media-body -->
                            <hr>
                            {{--<div class="clearfix mt20">--}}
                                {{--<div class="pull-left">--}}
                                    {{--<h5 class="md-title nomargin">Yesterday</h5>--}}
                                    {{--<h4 class="nomargin">10,009</h4>--}}
                                {{--</div>--}}
                                {{--<div class="pull-right">--}}
                                    {{--<h5 class="md-title nomargin">This Week</h5>--}}
                                    {{--<h4 class="nomargin">178,222</h4>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        </div><!-- panel-body -->
                    </div><!-- panel -->
                </div><!-- col-md-4 -->

                <div class="col-md-4">
                    <div class="panel panel-dark noborder">
                        <div class="panel-heading noborder">
                            <div class="panel-btns">
                                <a href="" class="panel-close tooltips" data-toggle="tooltip" data-placement="left" title="Close Panel"><i class="fa fa-times"></i></a>
                            </div><!-- panel-btns -->
                            <div class="panel-icon"><i class="fa fa-pencil"></i></div>
                            <div class="media-body">
                                <h5 class="md-title nomargin">Orders</h5>
                                <h3>Completed Orders</h3>
                                <h1 class="mt5">{{$order_completed}}</h1>
                            </div><!-- media-body -->
                            <hr>
                            {{--<div class="clearfix mt20">--}}
                                {{--<div class="pull-left">--}}
                                    {{--<h5 class="md-title nomargin">Yesterday</h5>--}}
                                    {{--<h4 class="nomargin">144,009</h4>--}}
                                {{--</div>--}}
                                {{--<div class="pull-right">--}}
                                    {{--<h5 class="md-title nomargin">This Week</h5>--}}
                                    {{--<h4 class="nomargin">987,212</h4>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        </div><!-- panel-body -->
                    </div><!-- panel -->
                </div><!-- col-md-4 -->
            </div><!-- row -->

        </div><!-- contentpanel -->

    </div><!-- mainpanel -->
@endsection
@section('foot')
    @parent
@endsection