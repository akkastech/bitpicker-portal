<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{url('user/assets/images/favicon.png')}}">

    <!-- App title -->
    <title>Sudanibit - Login</title>

    <!-- Bootstrap CSS -->
    <!-- Bootstrap CSS -->
    <link href="{{url('user/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- App CSS -->
    <link href="{{url('user/assets/css/style.css')}}" rel="stylesheet" type="text/css" />

    <!-- Modernizr js -->
    <script src="{{url('user/assets/js/modernizr.min.js')}}"></script>

</head>

<body>

<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="login-logo">
        <a href="{{url('/')}}">
            <img src="{{url('user/assets/images/sudani-bit.png')}}" />
        </a>
    </div>
    <div class="account-bg">
        <div class="card-box mb-0">
            <div class="m-t-10 p-10">
                <div class="row">
                    <div class="col-12 text-center">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {{session('success')}}
                            </div>
                        @endif
                            @if(session('error'))
                            <div class="alert alert-danger">
                                {{session('error')}}
                            </div>
                        @endif
                        <h6 class="text-uppercase m-b-0 m-t-0">Enter details below to Login!</h6>
                    </div>
                </div>
                <form class="m-t-20" method="post" action="{{url('authuser')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="email" required="" placeholder="Email" name="email">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="password" required="" placeholder="Password" name="password">
                        </div>
                    </div>

                    {{--<div class="form-group row">--}}
                        {{--<div class="col-12">--}}
                            {{--<div class="checkbox checkbox-custom">--}}
                                {{--<input id="checkbox-signup" type="checkbox">--}}
                                {{--<label for="checkbox-signup">--}}
                                    {{--Remember me--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group text-center row m-t-10">
                        <div class="col-12">
                            <button class="btn btn-success btn-block waves-effect waves-light" type="submit">LOGIN</button>
                        </div>
                    </div>

                    {{--<div class="form-group row m-t-15 mb-20 text-right">--}}
                        {{--<div class="col-12">--}}
                            {{--<a href="forget-password.html"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group row mb-0 text-center">
                        <div class="col-md-12 text-center">
                            <p>Don't have an account? <a href="{{url('register')}}" class="m-l-5"><b>Sign Up Here</b></a></p>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
    <!-- end card-box-->

</div>
<!-- end wrapper page -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{url('user/assets/js/jquery.min.js')}}"></script>
<script src="{{url('user/assets/js/popper.min.js')}}"></script><!-- Tether for Bootstrap -->
<script src="{{url('user/assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('user/assets/js/detect.js')}}"></script>
<script src="{{url('user/assets/js/waves.js')}}"></script>
<script src="{{url('user/assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('user/assets/plugins/switchery/switchery.min.js')}}"></script>

<!-- App js -->
<script src="{{url('user/assets/js/jquery.core.js')}}"></script>
<script src="{{url('user/assets/js/jquery.app.js')}}"></script>

</body>
</html>