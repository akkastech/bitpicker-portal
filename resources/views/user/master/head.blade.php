<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- App Favicon -->
<link rel="shortcut icon" href="{{url('user/assets/images/favicon.png')}}">

<!-- App title -->
<title>BitPicker</title>
<link href="{{url('user/assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
<!-- DataTables -->
<link href="{{url('user/assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('user/assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('user/assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- form Uploads -->
<link href="{{url('user/assets/plugins/fileuploads/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Bootstrap CSS -->
<link href="{{url('user/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
{{--<link href="{{url('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}

<!-- App CSS -->
<link href="{{url('user/assets/css/style.css')}}" rel="stylesheet" type="text/css" />

<!-- Modernizr js -->
<script src="{{url('user/assets/js/modernizr.min.js')}}"></script>

<style>
    .list-group-item.active{
        margin: 3px;
    }
    .list-group-item{
        margin: 3px;
    }
</style>