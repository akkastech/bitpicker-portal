<header id="topnav">
    <div class="topbar-main">
        <div class="container">
            <!-- LOGO -->
            <div class="topbar-left">
                <a href="{{url('user/dashboard')}}" class="logo"><img src="{{url('user/assets/images/sudani-bit.png')}}"/></a>
            </div>
            <!-- End Logo container-->

            <div class="menu-extras navbar-topbar">
                <ul class="list-inline float-right mb-0">
                    <li class="list-inline-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>

                    <li class="list-inline-item dropdown notification-list">
                        <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            <img src="{{url('files/images/'.Auth::user()->avatar)}}" alt="user" class="rounded-circle">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                            <!-- item-->
                            <a href="{{url('user/profile_edit')}}" class="dropdown-item notify-item">
                                <i class="zmdi zmdi-account-circle"></i> <span>Profile</span>
                            </a>
                            <!-- item-->
                            <a href="{{url('logout')}}" class="dropdown-item notify-item">
                                <i class="zmdi zmdi-power"></i> <span>Logout</span>
                            </a>
                        </div>
                    </li>

                </ul>

            </div> <!-- end menu-extras -->
            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    {{--<li class="active"><a href="{{url('user/dashboard')}}"><i class="zmdi zmdi-view-dashboard"></i> <span> </span> </a></li>--}}
                    <li><a href="{{url('user/dashboard')}}"><i class="zmdi zmdi-view-dashboard"></i><span> Dashboard  </span> </a></li>
                    <li><a href="{{url('user/order_form')}}"><i class="zmdi zmdi-collection-text"></i><span> Order Form </span> </a></li>
                    <li><a href="{{url('user/bank_info')}}"><i class="zmdi zmdi-collection-item"></i> <span> Bank Info </span> </a></li>
                </ul>
                <!-- End navigation menu  -->
            </div>
        </div>
    </div>
</header>