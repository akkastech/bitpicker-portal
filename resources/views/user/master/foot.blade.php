<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{url('user/assets/js/jquery.min.js')}}"></script>
<script src="{{url('user/assets/js/popper.min.js')}}"></script><!-- Tether for Bootstrap -->
<script src="{{url('user/assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('user/assets/js/waves.js')}}"></script>
<script src="{{url('user/assets/js/jquery.nicescroll.js')}}"></script>

<!-- Required datatable js -->
<script src="{{url('user/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('user/assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{url('user/assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{url('user/assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('user/assets/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{url('user/assets/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{url('user/assets/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{url('user/assets/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{url('user/assets/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{url('user/assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{url('user/assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{url('user/assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

<!-- Validation js (Parsleyjs) -->
<script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>

<!-- file uploads js -->
<script src="{{url('user/assets/plugins/fileuploads/js/dropify.min.js')}}"></script>
<!-- App js -->
<script src="{{url('user/assets/js/jquery.core.js')}}"></script>
<script src="{{url('user/assets/js/jquery.app.js')}}"></script>
<!-- Modal-Effect -->
<script src="{{url('user/assets/plugins/custombox/js/custombox.min.js')}}"></script>
<script src="{{url('user/assets/plugins/custombox/js/legacy.min.js')}}"></script>
<script src="{{url('user/assets/plugins/switchery/switchery.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('form').parsley();
    });
</script>

<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();

        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf']
        });

        table.buttons().container()
                .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    } );


</script>