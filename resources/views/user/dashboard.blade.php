@extends('user.layout')
@section('title','dashboard')
@section('content')
<div class="container">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Dashboard</h4>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                @if(session('success'))
                    <p class="alert alert-success" style="background: white">{{session('success')}}</p>
                @endif
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>BTC Amount</th>
                        <th>Wallet Number</th>
                        <th>Phone No.</th>
                        <th>Status</th>
                        {{--<th>Action</th>--}}
                    </tr>
                    </thead>

                    <tbody>
                    @if($userorders)
                    @foreach($userorders as $order)
                    <tr>
                        <td>{{date($order->created_at)}}</td>
                        <td>{{$order->amount_btc}}</td>
                        <td>{{$order->btc_wallet}}</td>
                        <td>{{$order->phone}}</td>
                        <td>
                            @if($order->status == 'Nothing')
                                <a href="#" class="btn btn-dark receipt_button waves-effect waves-light btn-sm" data-id="{{$order->order_id}}"  data-toggle="modal" data-target="#myModal">Upload Receipt<span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></a>
                            @elseif($order->status == 'Rejected')
                                <span class="label label-danger">Rejected</span>&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-dark receipt_button waves-effect waves-light btn-sm" data-id="{{$order->order_id}}"  data-toggle="modal" data-target="#myModal">Upload Again<span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></a>
                           @elseif($order->status == 'Accepted')
                                <span class="label label-primary">Accepted</span>
                            @elseif($order->status == 'Pending')
                                <span class="label label-warning">Pending</span>
                            @elseif($order->status == 'Completed')
                                <span class="label label-success">Completed</span>
                            @endif
                        </td>
{{--                        <td><span @if($order->status == 'Nothing') class="label label-default" @elseif($order->status == 'Rejected') class="label label-warning"   @elseif($order->status == 'Accepted') class="label label-info" @elseif($order->status == 'Completed') class="label label-success"@endif>{{$order->status}}</span></td>--}}
                        {{--<td>{{$order->created_at}}</td>--}}







                        {{--<td>--}}
                           {{--@if($order->document == NULL && $order->status == 'Nothing')--}}
                                {{--<a href="#" class="btn btn-dark waves-effect waves-light btn-sm"  data-toggle="modal" data-target="#myModal">Upload Receipt<span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></a>--}}
                                {{--<form method="post" action="{{url('user/document_save/'.$order->order_id)}}" enctype="multipart/form-data">--}}
                                    {{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}
                                    {{--<div class="btn-group form-group">--}}
                                        {{--<input type="file" class="filestyle" data-buttonText="Find file" name="document" data-buttonName="btn-primary" accept=".pdf,.doc" required>&nbsp;&nbsp;&nbsp;&nbsp;--}}
                                        {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
                                    {{--</div>--}}
                                {{--</form>--}}
                           {{--@elseif($order->status == 'Pending')--}}
                                {{--<p class="text-success text-center">Uploaded</p>--}}
                                {{--@elseif($order->status == 'Rejected')--}}
                                {{--<a href="#" class="btn btn-dark waves-effect waves-light btn-sm"  data-toggle="modal" data-target="#myModal">Upload Again<span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></a>--}}
                              {{--@elseif($order->status == 'Completed')--}}
                                {{--<a class="btn btn-success disabled">Completed</a>--}}
                                {{--<div class="bg-default text-primary center">Uploaded</div>--}}
                                {{--<p class="text-info center">Uploaded</p>--}}
                            {{--@endif--}}
                        {{--</td>--}}

                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end row -->

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <form action="{{url('user/document_save')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="order_id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Upload Document</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <label>Upload File</label>
                        <input required type="file" class="filestyle" data-buttonText="Find file" name="document" data-buttonName="btn-primary" accept=".pdf,image/*">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </form>
            </div>
        </div>

</div> <!-- container -->
@endsection
@section('foot')
    @parent
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>--}}
    {{--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--}}
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-filestyle/1.3.0/bootstrap-filestyle.js"> </script>
    <script>
        $('.filecount').filestyle({
            input : false,
            text:'Upload Document',
            buttonName : 'btn-danger',
            iconName : 'glyphicon glyphicon-folder-close'
        });
        $('.receipt_button').on('click', function(){
            var id = $(this).data('id');
            $('#myModal').find('input[name=order_id]').val(id);
        });
        </script>
@endsection