@extends('user.layout')
@section('title','dashboard')
@section('content')
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Your Profile Settings</h4>
            </div>
        </div>
        <!-- end row -->

        <div class="row">

            <div class="col-md-6">

                <div class="card-box">
                    @if(session('success'))
                        <h5 class="alert alert-success" style="background-color: whitesmoke;"><b>{{session('success')}}</b></h5>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger">
                            <b>{{session('error')}}</b>
                        </div>
                    @endif
                    <img src="{{url('files/images/'.Auth::user()->avatar)}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px; margin-bottom:10px;">
                    <h2> {{Auth::user()->name }}'s Profile</h2>
                    <form class="form-horizontal form-material" id="loginform" action="{{url('user/profile_update/'.Auth::user()->id)}}" enctype="multipart/form-data" method="post">
                        <div class="col-md-12">
                        <label>Update Profile Image</label>
                        <input type="file" name="avatar">
                            </div></br></br></br>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-12">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder=" User name" name="name" value="{{Auth::user()->name}}">
                                </div>
                            </div>
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <input class="form-control" type="email" required="" placeholder="Email" name="email" value="{{Auth::user()->email}}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary text-uppercase pull-right">Update</button>
                            </div>
                        </div>
                    </form>

                    <div class="panel panel-primary" style="margin-bottom:0px;">
                        <div class="panel-heading">Change Password</div>
                    </div>
                    <br>
                    <form id="form-change-password" role="form" method="POST" action="{{ url('user/change_password/'.\Illuminate\Support\Facades\Auth::user()->id) }}"  class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-12">
                            <label for="current-password" class="col-sm-2 control-label">Current Password</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <input type="password" value="" class="form-control" id="current-password" name="current_password" placeholder="Password" required="">
                                </div>
                            </div>
                            <label for="password" class="col-sm-2 control-label">New Password</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                                </div>
                            </div>
                            <label for="password_confirmation" class="col-sm-2 control-label">Re-enter Password</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-danger text-uppercase pull-right">Change</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- container -->
@endsection
@section('foot')
    @parent
@endsection