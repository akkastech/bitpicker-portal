@extends('user.layout')
@section('title','dashboard')
@section('content')
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Bank Information</h4>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-md-6">
                <div class="card-box">
                    <div class="form-group m-b-0 m-t-10">
                        {{--<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.--}}
                            {{--Curabitur arcu erat, accumsan id aimperdiet et, porttitor at sem. Nulla porttitor accumsan tincidunt.</p>--}}

                        {{--<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.--}}
                            {{--Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Nulla porttitor accumsan tincidunt.</p>--}}
                        <div class="bs-example row" style="margin: 20px;">
                            <div class="list-group col-md-6" style="width: 200px;">
                                <a href="#" class="list-group-item active">
                                    <span class="glyphicon glyphicon-camera">Title</span>
                                </a>
                                <a href="#" class="list-group-item active">
                                    <span class="glyphicon glyphicon-file">Account Number</span>
                                </a>
                                <a href="#" class="list-group-item active">
                                    <span class="glyphicon glyphicon-music">Bank Name</span>
                                </a>
                            </div>
                            <div class="list-group col-md-6" style="width: 200px;">
                                <a href="#" class="list-group-item">
                                    <span class="glyphicon glyphicon-camera">@if($bank_info){{$bank_info->title}} @endif</span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <span class="glyphicon glyphicon-file">@if($bank_info){{$bank_info->account_num}}@endif</span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <span class="glyphicon glyphicon-music">@if($bank_info){{$bank_info->bank_name}}@endif</span>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div><!-- end col-->
        </div>
        <!-- end row -->
    </div> <!-- container -->
@endsection
@section('foot')
    @parent
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">--}}
@endsection