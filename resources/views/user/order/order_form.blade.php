@extends('user.layout')
@section('title','dashboard')
@section('content')
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Order</h4>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-md-6">
                <div class="card-box">
                    <div class="p-20">
                        <form data-parsley-validate method="post" action="{{url('user/order_save')}}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="userName">Name<span class="text-danger">*</span></label>
                                <input type="text" name="name" parsley-trigger="change" required
                                       placeholder="Enter Name" class="form-control" id="userName" value="{{Auth::user()->name}}">
                            </div>
                            <div class="form-group">
                                <label for="emailAddress">Email<span class="text-danger">*</span></label>
                                <input type="email" name="email" parsley-trigger="change" required
                                       placeholder="Enter Email" class="form-control" id="emailAddress" value="{{Auth::user()->email}}">
                            </div>
                            <div class="form-group">
                                <label for="phone1">Phone #<span class="text-danger">*</span></label>
                                <input id="phone1" type="tel" placeholder="00-000-0000" required
                                       class="form-control numbersOnly" name="phone" maxlength="11">
                            </div>
                            <div class="form-group">
                                <label for="">Amount in BTC<span class="text-danger">*</span></label>
                                <input type="text" name="amount_btc" parsley-trigger="change" required
                                       placeholder="Enter Amount" class="form-control numbersOnly" id="amount">
                            </div>
                            <div class="form-group">
                                <label for="">BTC Wallet #<span class="text-danger">*</span></label>
                                <input type="text" name="btc_wallet" parsley-trigger="change" required
                                       placeholder="Enter Wallet Number" class="form-control" id="walletNumber">
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="">Upload Document<span class="text-danger">*</span></label>--}}
                                {{--<input type="file" class="dropify" data-height="300" name="document"/>--}}
                            {{--</div>--}}

                            <div class="form-group text-right m-b-0 m-t-40">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                <a href="#" type="reset" class="btn btn-secondary waves-effect m-l-5"> Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- end col-->
        </div>
        <!-- end row -->

    </div> <!-- container -->
    @endsection
@section('foot')
    @parent
    <script>
        jQuery('.numbersOnly').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g,'');
        });
    </script>
@endsection