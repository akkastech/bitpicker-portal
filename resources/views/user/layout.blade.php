<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    @include('user.master.head')
</head>
<body>
@include('user.master.header')
<!-- START CONTENT -->
<section>
<div class="wrapper">
    @yield('content')
    @include('user.master.footer')
</div>
</section>
@section('foot')
    @include('user.master.foot')
@show
<!-- End Content -->
</body>
</html>