<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link href="{{url('assets/css/bootstrap.css')}}" rel="stylesheet">
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3" style="margin-top: 100px;">
			@if(session('error'))
				<p class="alert alert-danger">{{session('error')}}</p>
			@endif
			<h4>Subscribe List</h4>
		</div>
			
	</div>

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<table class="table table-bordered">
				<thead>
                    <tr>
                        <th>#</th>
                        <th>Email</th>
                        <th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($emails) > 0)

                        @foreach($emails as $key => $email)

                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$email->email}}</td>
                                <td>{{date('y-M-d', strtotime($email->created_at))}}</td>
                                
                            </tr>

                        @endforeach

                    @else
                        <tr>
                            <td colspan="3">No Record Found</td>
                        </tr>
                    @endif
                </tbody>
			</table>
		</div>
	</div>
	
</div>



<script src="{{url('assets/js/jquery.min.js')}}"></script> 
<script src="{{url('assets/js/bootstrap.min.js')}}"></script> 
</body>
</html>