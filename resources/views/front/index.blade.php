@extends('front.layout')


@section('title', 'Home')


@section('content')

  <div id="exchange" class="section">
        <div class="container inner">
        <h2 class="section-title text-center">CURRENCY  EXCHANGE COMPARISON</h2>
    <div class="head-line"></div>
        <div class="row">
          @if(session('error'))
            <p class="alert alert-error" style="background: white">{{session('success')}}</p>
          @endif
      <div class="col-sm-12" id="exchange-amount">
         
      {{--<div class="tabs tabs-top center tab-container">--}}
            {{--<ul class="etabs">--}}
              {{--<li class="tab active"><a class="@if(isset($request->type)) @if($request->type == 'BUY') active @endif @endif" href="#tab-1">BUY</a></li>--}}
              {{--<li class="tab sell_tab"><a class="@if(isset($request->type)) @if($request->type == 'SELL') active @endif @endif" href="#tab-2">SELL</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        <div class="col-md-12 text-center">
          <a class="btn btn-success bm50" href="{{url('user/order_form')}}">BUY NOW</a>
        </div>
            <!-- /.etabs -->
      <form action="{{url('search/companies')}}" method="get">
      <input type="hidden" name="type" @if(isset($request->type)) value="{{$request->type}}" @else value="BUY" @endif">
      <div class="panel-container">
        <div class="tab-block" id="tab-1">
          <div class="enter-box-1 coins-amount">
            <h4>AMOUNT</h4>
            <input type="text" name="buy_amount" class="text-input amount_input defaultText" value="@if(isset($request->type)) @if($request->type == 'BUY') {{$request->buy_amount}} @endif @endif" placeholder="0.00" required="">
          </div>
          <div class="enter-box-2">
            <i class="budicon-arrow-right"></i>
          </div>
          <div class="enter-box-1 coins-amount">
            <h4>CHOOSE YOUR CURRENCY</h4>
            <select class="CURRENCY" name="buy_currency" required="" style="width:100%">
              <option value="USD"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'USD') selected @endif   @endif   >USD</option>
              <option value="GBP"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'GBP') selected @endif   @endif   >GBP</option>
              <option value="EUR"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'EUR') selected @endif   @endif   >EUR</option>
              <option value="CHF"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'CHF') selected @endif   @endif   >CHF</option>
              <option value="CAD"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'CAD') selected @endif   @endif   >CAD</option>
              <option value="AUD"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'AUD') selected @endif   @endif   >AUD</option>
              <option value="INR"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'INR') selected @endif   @endif   >INR</option>
              <option value="JPY"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'JPY') selected @endif   @endif   >JPY</option>
              <option value="KWD"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'KWD') selected @endif   @endif   >KWD</option>
              <option value="CNY"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'CNY') selected @endif   @endif   >CNY</option>
              <option value="SEK"  @if(isset($request->buy_currency)) @if($request->buy_currency == 'SEK') selected @endif   @endif   >SEK</option>
            </select>
          </div>
          <div class="enter-box-2">
            <i class="budicon-arrow-right"></i>
          </div>
          <div class="enter-box-1 coins-amount">
            <h4>CHOOSE CRYPTOCURRENCY</h4>
            <select class="CRYPTOCURRENCY" name="buy_crypto_currency" required="" style="width:100%">
              @if(count($currencies) > 0)
                @foreach($currencies as $cur)
                  <option value="{{$cur->name}}" @if(isset($request->buy_crypto_currency)) @if($request->buy_crypto_currency == $cur->name) selected @endif   @endif >{{$cur->name}}</option>
                @endforeach
              @endif
            </select>
          </div>
        </div>
        <!-- /.tab-block -->
        
             {{--  <div class="tab-block" id="tab-2">
                <div class="enter-box-1 coins-amount">
                  <h4>CHOOSE CRYPTOCURRENCY</h4>
                  <select class="CRYPTOCURRENCY" style="width:100%;" name="sell_crypto_currency" >
                    @if(count($currencies) > 0)
                      @foreach($currencies as $cur)
                        <option value="{{$cur->name}}" @if(isset($request->sell_crypto_currency)) @if($request->sell_crypto_currency == $cur->name) selected @endif   @endif >{{$cur->name}}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
                <div class="enter-box-2">
                  <i class="budicon-arrow-right"></i>
                </div>
                <div class="enter-box-1 coins-amount">
                  <h4>AMOUNT</h4>
                  <input type="text" name="sell_amount" class="text-input amount_input defaultText" value="@if(isset($request->type)) @if($request->type == 'SELL') {{$request->sell_amount}} @endif @endif" placeholder="0.00" >
                </div>
                <div class="enter-box-2">
                  <i class="budicon-arrow-right"></i>
                </div>
                <div class="enter-box-1 coins-amount">
                  <h4>CHOOSE YOUR CURRENCY</h4>
                  <select class="CURRENCY" name="sell_currency" required="" style="width:100%">
                    <option value="USD"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'USD') selected @endif   @endif   >USD</option>
                    <option value="GBP"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'GBP') selected @endif   @endif   >GBP</option>
                    <option value="EUR"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'EUR') selected @endif   @endif   >EUR</option>
                    <option value="CHF"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'CHF') selected @endif   @endif   >CHF</option>
                    <option value="CAD"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'CAD') selected @endif   @endif   >CAD</option>
                    <option value="AUD"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'AUD') selected @endif   @endif   >AUD</option>
                    <option value="INR"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'INR') selected @endif   @endif   >INR</option>
                    <option value="JPY"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'JPY') selected @endif   @endif   >JPY</option>
                    <option value="KWD"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'KWD') selected @endif   @endif   >KWD</option>
                    <option value="CNY"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'CNY') selected @endif   @endif   >CNY</option>
                    <option value="SEK"  @if(isset($request->SELL_currency)) @if($request->SELL_currency == 'SEK') selected @endif   @endif   >SEK</option>
                    
                  </select>
                </div>
              </div> --}}
              <!-- /.tab-block -->
            </div>
            <!-- /.panel-container --> 
          </div>
          <!-- /.tabs --> 
        </div>

        </div>
       
        <div class="row">
          <div class="col-md-12" style="margin-bottom:10px;"><h4>CHOOSE EXCHANGE</h4></div>
            <div class="col-md-12" id="choose-exchange">
              <div class="grey-wrapper">
                <ul>
                  <li>
                  <div class="checkbox1">
                    <input type="checkbox" value="POLONIEX"  class="check_array" id="checkbox1Input" @if(isset($request->companies)) @if(in_array('POLONIEX', $request->companies)) checked @endif checked=""  @endif  name="companies[]"/>
                    <label for="checkbox1Input"></label>
                  </div>
                  <div>POLONIEX</div>
                  </li>
                  <li>
                  <div class="checkbox2">
                    <input type="checkbox" value="KRAKEN" class="check_array" id="checkbox2Input" @if(isset($request->companies)) @if(in_array('KRAKEN', $request->companies)) checked @endif   @endif  name="companies[]"/>
                    <label for="checkbox2Input"></label>
                  </div>
                  <div>KRAKEN</div>
                  </li>
                  <li>
                  <div class="checkbox3">
                    <input type="checkbox" value="BITTREX" class="check_array" id="checkbox3Input" @if(isset($request->companies)) @if(in_array('BITTREX', $request->companies)) checked @endif   @endif  name="companies[]"/>
                    <label for="checkbox3Input"></label>
                  </div>
                  <div>BITTREX</div>
                  </li>
                  <li>
                  <div class="checkbox4">
                    <input type="checkbox" value="BITFINEX" class="check_array" id="checkbox4Input" @if(isset($request->companies)) @if(in_array('BITFINEX', $request->companies)) checked @endif   @endif  name="companies[]"/>
                    <label for="checkbox4Input"></label>
                  </div>
                  <div>BITFINEX</div>
                  </li>
                  <li>
                  <div class="checkbox5">
                    <input type="checkbox" value="HITBTC" class="check_array" id="checkbox5Input" @if(isset($request->companies)) @if(in_array('HITBTC', $request->companies)) checked @endif   @endif  name="companies[]"/>
                    <label for="checkbox5Input"></label>
                  </div>
                  <div>HITBTC</div>
                  </li>

                </ul>
              </div>
        
        
            </div>
            <div class="col-md-12 text-center m-t-15">
              <button class="bm0 m-t-20" type="submit" id="run">SEARCH</button>
            </div>
          </div>
      </form>
    
    

      

      @if($request != false)

          <div class="row animated bounceIn" id="exchange-result">
          <div class="col-md-12">
            <div class="grey-wrapper">
              <h2 class="text-center">BEST EXCHANGE RATE</h2>
              <div class="head-line"></div>
              @if(count($final_array) > 0)
                  @foreach($final_array as $key => $final)

                      @if($check_price == $final['price'])

                        <div class="results">
                          <div class="box-2 your-amount">
                            <h4>YOUR AMOUNT</h4>
                            <h3>@if($request->type == 'BUY') {{$request->buy_amount}} @else {{$request->sell_amount}} @endif</h3>
                          </div>
                          <div class="box-2 your-currency">
                            <h4>YOUR CURRENCY</h4>
                            <h3>@if($request->type == 'BUY') {{$request->buy_currency}} @else {{$request->sell_currency}} @endif</h3>
                          </div>
                          <div class="box chosen-crypto-currency" style="width: 180px;">
                            <h4>CHOOSEN CRYPTOCURRENCY</h4>
                            <h3>@if($request->type == 'BUY') {{$request->buy_crypto_currency}} @else {{$request->sell_crypto_currency}} @endif</h3>
                          </div>
                          <div class="box-3 sm-icon-2">
                            <i class="budicon-arrow-right-1"></i>
                          </div>
                          <div class="box-4 chosen-crypto-currency">
                            <h4>CALCULATED {{$key}} RATE</h4>
                            <h3>{{$final['price']}}</h3>
                          </div>
                          <div class="box-2 your-currency pull-right" style="margin-right: 0px;margin-left: 15px;width:150px;margin-top: 5px;">
                            <a href="https://{{mb_strtolower($key)}}.com/" target="_blank"><img src="{{url('assets/images/'.mb_strtolower($key).'-ic.png')}}" /></a>
                            <h3>{{$key}}</h3>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12 text-center m-t-15">
                            <a href="https://{{mb_strtolower($key)}}.com/" target="_blank" class="btn m-t-10">TAKE ME!</a>
                          </div>
                        </div>

                      @endif

                      

                  @endforeach

              @else

                <h3>Record Not Found</h3>

              @endif
              @if(isset($check_price))
                @if($check_price == "")
                  <h3>Record Not Found</h3>
                @endif
              @endif
            </div>
          </div>
        </div>

      @endif


    
    

      </div>
  </div>
  <!-- /#exchange-->

  @if($request == false)

    <div id="counters" class="section"> 
      <div class="container mb-text-center">
    <div class="outter-box mb-b-30">
      <div class="sm-icon"><img src="{{url('assets/images/poloniex-ic.png')}}" /></div>
      <h3>POLONIEX</h3>
      <div class="rate-counter">
         <div class="large-meter">
          <div class="odometer f-36 poloniex-ask">{{$data['poloniex']['ask']}}</div>
        </div>
        <div class="small-meter">
          <div class="odometer2 f-18 poloniex-point">{{$data['poloniex']['point']}}</div>
        </div>
      </div>
      <div class="currency">BTC/USD</div>
      <div class="stats increase">N/A</div>
        </div>
    
    <div class="outter-box mb-b-30">
      <div class="sm-icon"><img src="{{url('assets/images/kraken-ic.png')}}" /></div>
      <h3>KRAKEN</h3>
      <div class="rate-counter">
        <div class="large-meter">
          <div class="odometer f-36 kraken-ask">{{$data['kraken']['ask']}}</div>
        </div>
        <div class="small-meter">
          <div class="odometer2 f-18 kraken-point">{{$data['kraken']['point']}}</div>
        </div>
      </div>
      <div class="currency">BTC/USD</div>
      <div class="stats decrease kraken-avrage" style="color: {{$data['kraken']['color']}}">{{$data['kraken']['avrage']}}</div>
        </div>
        
    <div class="outter-box mb-b-30">
      <div class="sm-icon"><img src="{{url('assets/images/bittrex-ic.png')}}" /></div>
      <h3>BITTREX</h3>
      <div class="rate-counter">
        <div class="large-meter">
          <div class="odometer f-36 bittrex-ask">{{$data['bittrex']['ask']}}</div>
        </div>
        <div class="small-meter">
          <div class="odometer2 f-18 bittrex-point">{{$data['bittrex']['point']}}</div>
        </div>
      </div>
      <div class="currency">BTC/USD</div>
      <div class="stats increase bittrex-avrage" style="color:{{$data['bittrex']['color']}}">{{$data['bittrex']['avrage']}}</div>
        </div>
    
    <div class="outter-box mb-b-30">
      <div class="sm-icon"><img src="{{url('assets/images/bitfinex-ic.png')}}" /></div>
      <h3>BITFINEX</h3>
      <div class="rate-counter">
        <div class="large-meter">
          <div class="odometer f-36 bitfinex-ask">{{$data['bitfinex']['ask']}}</div>
        </div>
        <div class="small-meter">
          <div class="odometer2 f-18 bitfinex-point">{{$data['bitfinex']['point']}}</div>
        </div>
      </div>
      <div class="currency">BTC/USD</div>
      <div class="stats increase bitfinex-avrage" style="{{$data['bitfinex']['color']}}">{{$data['bitfinex']['avrage']}}</div>
        </div>
    
    <div class="outter-box-last">
      <div class="sm-icon"><img src="{{url('assets/images/hitbtc-ic.png')}}" /></div>
      <h3>HITBTC</h3>
      <div class="rate-counter">
        <div class="large-meter">
          <div class="odometer f-36 hitbtc-ask">{{$data['hitbtc']['ask']}}</div>
        </div>
        <div class="small-meter">
          <div class="odometer2 f-18 hitbtc-point">{{$data['hitbtc']['point']}}</div>
        </div>
      </div>
      <div class="currency">BTC/USD</div>
      <div class="stats decrease hitbtc-avrage" style="color:{{$data['hitbtc']['color']}}">{{$data['hitbtc']['avrage']}}</div>
        </div>
    
      </div>
  </div>  <!-- /#counters -->

  @else


    <div id="counters" class="section"> 
      <div class="container mb-text-center">

        @if(count($data) > 0)

          @foreach($data as $key => $res)

              <div class="outter-box mb-b-30" style="margin-right: 15px;">
                <div class="sm-icon"><img src="{{url('assets/images/'.mb_strtolower($key).'-ic.png')}}" /></div>
                <h3>{{strtoupper($key)}}</h3>
                <div class="rate-counter">
                   <div class="large-meter">
                    <div class="odometer f-36">{{$res['ask']}}</div>
                  </div>
                  <div class="small-meter">
                    <div class="odometer2 f-18">{{$res['point']}}</div>
                  </div>
                </div>
                <div class="currency">
                  @if($request->type == "BUY")
                    {{$request->buy_crypto_currency}}/{{$request->buy_currency}}
                  @elseif($request->type == "SELL")
                    {{$request->sell_crypto_currency}}/{{$request->sell_currency}}
                  @endif
                </div>
                {{-- <div class="stats increase">N/A</div> --}}
              </div>

          @endforeach

        @endif

      </div>
  </div>  <!-- /#counters -->


  @endif

  <div id="donate" class="section">
      <div class="container inner">  
    
      {{--<div class="row text-center">--}}
      {{--<div class="col-md-12">--}}
        {{--<div class="donate-icon">--}}
          {{--<img src="{{url('assets/images/donate.png')}}" />--}}
        {{--</div>--}}
        {{--<button class="btn-large" data-toggle="modal" data-target="#myModal">DONATE</button><br>--}}
        {{-- <div class="m-t-15"><small><strong> COMING SOON! </strong></small></div> --}}
      {{--</div>--}}
        {{--</div>--}}
    
        <div class="row mb-m-t-60">
      <div class="col-md-6">
        <form action="{{url('subscribe')}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="subscribe">
            @if(session('success'))
              <p class="alert alert-success">{{session('success')}}</p>
            @endif
            <p>Subscribe to RECEIVE FUTURE UPDATES TO OUR SITE!</p>
            <div class="width-80">
              <input type="email" name="email" class="text-input defaultText required email" required="" placeholder="Enter email here ..."/>
            </div>
              <button class="bm0" type="submit">SUBSCRIBE</button>
          </div>
        </form>
      </div>
      <div class="col-md-6">
        <div class="contact">
          <p>If you have ANY questions OR FEEDBACK, PLEASE CONTACT US AND WE WILL GET BACK TO YOU AS SOON AS POSSIBLE.</p>
          <button class="m-t-30">CONTACT US</button>
        </div>
      </div>
        </div>
      </div>
  </div>
  <!-- /#donate -- contact -->



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{-- <h4 class="modal-title" id="myModalLabel">Donate Now</h4> --}}
      </div>
      <div class="modal-body">
        ...
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default">Donate</button>
      </div> --}}
    </div>
  </div>
</div>

@stop

@section('foot')
	@parent
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){

      // if('.check_array').on('change', function(){
      //   var checked = $('input[name="companies[]"]:checked').length;
      //   var count = 0;
      //   console.log(checked);
      //   $('.check_array').each(function(){
      //     console.log($(this).val())
      //     if($(this).is(':checked') == true){
      //       count++;
      //     }
      //   });
      //   console.log(count);
      // }); 

       $(".amount_input").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


      @if(session('success'))
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        setTimeout(function(){ $('.alert-success').remove(); }, 5000);
      @endif
      $('.CRYPTOCURRENCY').select2();
      $('.CURRENCY').select2();

      @if(isset($request->type))
        @if($request->type == 'SELL')
          $('.sell_tab').find('a').click();
        @endif
      @endif

      $('.tab').on('click', function(){

        var type = $(this).find('a').html();

        $('input[name=type]').val(type);
        console.log(type);
        if(type == 'BUY'){
          $('input[name=sell_amount]').attr('required', false);
          $('input[name=sell_crypto_currency]').attr('required', false);
          $('input[name=sell_currency]').attr('required', false);

          $('input[name=buy_amount]').attr('required', true);
          $('input[name=buy_crypto_currency]').attr('required', true);
          $('input[name=buy_currency]').attr('required', true);
        }else if(type == 'SELL'){
          $('input[name=sell_amount]').attr('required', true);
          $('input[name=sell_crypto_currency]').attr('required', true);
          $('input[name=sell_currency]').attr('required', true);

          $('input[name=buy_amount]').attr('required', false);
          $('input[name=buy_crypto_currency]').attr('required', false);
          $('input[name=buy_currency]').attr('required', false);
        }

      });

    });
  </script>
	<script>
        $(document).ready(function () {

          @if($request == false)
            setInterval(all_ajax, 3000);
          @endif

        });

      @if($request == false)
        function all_ajax()
        {
            $.ajax({
              type: "get",
              url: '{{ url("all/ajax/calls")}}',
              dataType: ''
            }).success(function(res) {
              
              $('.kraken-ask').html(res.kraken.ask);
              if(res.kraken.point.length == 2){
                $('.kraken-point').html(res.kraken.point);
              }else{
                var point = '0'+res.kraken.point;
                $('.kraken-point').html(point);
              }
              $('.kraken-avrage').html(res.kraken.avrage);
              $('.kraken-avrage').css('color', res.kraken.color);

              $('.bittrex-ask').html(res.bittrex.ask);
              if(res.bittrex.point.length == 2){
                $('.bittrex-point').html(res.bittrex.point);
              }else{
                var point = '0'+res.bittrex.point;
                $('.bittrex-point').html(point);
              }
              $('.bittrex-avrage').html(res.bittrex.avrage);
              $('.bittrex-avrage').css('color', res.bittrex.color);

              $('.bitfinex-ask').html(res.bitfinex.ask);
              if(res.bitfinex.point.length == 2){
                $('.bitfinex-point').html(res.bitfinex.point);
              }else{
                var point = '0'+res.bitfinex.point;
                $('.bitfinex-point').html(point);
              }
              $('.bitfinex-avrage').html(res.bitfinex.avrage);
              $('.bitfinex-avrage').css('color', res.bitfinex.color);

              $('.hitbtc-ask').html(res.hitbtc.ask);
              if(res.hitbtc.point.length == 2){
                $('.hitbtc-point').html(res.hitbtc.point);
              }else{
                var point = '0'+res.hitbtc.point;
                $('.hitbtc-point').html(point);
              }
              $('.hitbtc-avrage').html(res.hitbtc.avrage);
              $('.hitbtc-avrage').css('color', res.hitbtc.color);

              $('.poloniex-ask').html(res.poloniex.ask);
              if(res.poloniex.point.length == 2){
                $('.poloniex-point').html(res.poloniex.point);
              }else{
                var point = '0'+res.poloniex.point;
                $('.poloniex-point').html(point);
              }

            });
        }
      @endif
    </script>

@endsection