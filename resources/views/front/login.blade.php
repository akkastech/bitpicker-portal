<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link href="{{url('assets/css/bootstrap.css')}}" rel="stylesheet">
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3" style="margin-top: 100px;">
			@if(session('error'))
				<p class="alert alert-danger">{{session('error')}}</p>
			@endif
			<h4>Enter Password</h4>
		</div>
		<form action="{{url('userauth')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="col-md-6 col-md-offset-3 form-group">
				<input type="password" name="password" required="" class="form-control">
			</div>
			
	</div>
	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="col-md-2 pull-right">
					<button class="btn btn-info">Login</button>
				</div>
			</div>
		</div>
	</form>
</div>



<script src="{{url('assets/js/jquery.min.js')}}"></script> 
<script src="{{url('assets/js/bootstrap.min.js')}}"></script> 
</body>
</html>