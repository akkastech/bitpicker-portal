<script src="{{url('assets/js/jquery.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/twitter-bootstrap-hover-dropdown.min.js')}}"></script>
<script src="{{url('assets/js/jquery.themepunch.plugins.min.js')}}"></script>
<script src="{{url('assets/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{url('assets/js/jquery.easytabs.min.js')}}"></script>
<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{url('assets/js/jquery.isotope.min.js')}}"></script>
<script src="{{url('assets/js/jquery.fitvids.js')}}"></script>
<script src="{{url('assets/js/jquery.fancybox.pack.js')}}"></script>
<script src="{{url('assets/js/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.2')}}"></script>
<script src="{{url('assets/js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.0')}}"></script>
<script src="{{url('assets/js/jquery.slickforms.js')}}"></script>
<script src="{{url('assets/js/instafeed.min.js')}}"></script>
<script src="{{url('assets/js/retina.js')}}"></script>
<script src="{{url('assets/js/google-code-prettify/prettify.js')}}"></script>
<script src="{{url('assets/js/scripts.js')}}"></script>


<script src="{{url('assets/js/particles/particles.js')}}"></script>
<script>
  
  /* -----------------------------------------------
/* How to use? : Check the GitHub README
/* ----------------------------------------------- */

/* To load a config file (particles.json) you need to host this demo (MAMP/WAMP/local)... */
/*
particlesJS.load('particles-js', 'particles.json', function() {
  console.log('particles.js loaded - callback');
});
*/

/* Otherwise just put the config content (json): */

particlesJS('particles-js',
  
    {
      "particles": {
        "number": {
          "value": 90,
          "density": {
            "enable": true,
            "value_area": 900
          }
        },
        "color": {
          "value": "#0453a2"
        },
        "shape": {
          "type": "circle",
          "stroke": {
            "width": 0,
            "color": "#000000"
          },
          "polygon": {
            "nb_sides": 5
          },
          "image": {
            "src": "img/github.svg",
            "width": 100,
            "height": 100
          }
        },
        "opacity": {
          "value": 0.5,
          "random": false,
          "anim": {
            "enable": false,
            "speed": 1,
            "opacity_min": 0.1,
            "sync": false
          }
        },
        "size": {
          "value": 5,
          "random": true,
          "anim": {
            "enable": false,
            "speed": 40,
            "size_min": 0.1,
            "sync": false
          }
        },
        "line_linked": {
          "enable": true,
          "distance": 150,
          "color": "#0453a2",
          "opacity": 0.4,
          "width": 1
        },
        "move": {
          "enable": true,
          "speed": 6,
          "direction": "none",
          "random": false,
          "straight": false,
          "out_mode": "out",
          "attract": {
            "enable": false,
            "rotateX": 600,
            "rotateY": 1200
          }
        }
      },
      "interactivity": {
        "detect_on": "canvas",
        "events": {
          "onhover": {
            "enable": true,
            "mode": "repulse"
          },
          "onclick": {
            "enable": true,
            "mode": "push"
          },
          "resize": true
        },
        "modes": {
          "grab": {
            "distance": 400,
            "line_linked": {
              "opacity": 1
            }
          },
          "bubble": {
            "distance": 400,
            "size": 40,
            "duration": 2,
            "opacity": 8,
            "speed": 3
          },
          "repulse": {
            "distance": 200
          },
          "push": {
            "particles_nb": 4
          },
          "remove": {
            "particles_nb": 2
          }
        }
      },
      "retina_detect": true,
      "config_demo": {
        "hide_card": false,
        "background_color": "#b61924",
        "background_image": "",
        "background_position": "50% 50%",
        "background_repeat": "no-repeat",
        "background_size": "cover"
      }
    }

  );

</script>


<script src="{{url('assets/js/odometer.js')}}"></script>



<script>

  $('.click-to-off').on('click', function(){

    var status = $(this).data('status');
    if(status == '0'){
      window.pJSDom[0].pJS.interactivity.events.onhover.enable = false;
      $(this).data('status', '1');
    }else{
      window.pJSDom[0].pJS.interactivity.events.onhover.enable = true;
      $(this).data('status', '0');
    }
      window.pJSDom[0].pJS.fn.particlesRefresh();

  });
</script>

