<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="{{url('assets/images/bit-favicon.png')}}">
<title>Sudanibit</title>
<!-- Bootstrap core CSS -->
<link href="{{url('assets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{url('assets/css/settings.css')}}" rel="stylesheet">
<link href="{{url('assets/css/owl.carousel.css')}}" rel="stylesheet">
<link href="{{url('assets/js/google-code-prettify/prettify.css')}}" rel="stylesheet">
<link href="{{url('assets/js/fancybox/jquery.fancybox.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{url('assets/js/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.2')}}" rel="stylesheet" type="text/css" />

<link href="{{url('assets/css/style.css')}}" rel="stylesheet">

<link href="{{url('assets/css/odometer-theme-car.css')}}" rel="stylesheet">
<link href="{{url('assets/css/animate.css')}}" rel="stylesheet">

<link href="{{url('assets/css/particles/style.css')}}" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800,900' rel='stylesheet' type='text/css'>

<link href="{{url('assets/type/fontello.css')}}" rel="stylesheet">
<link href="{{url('assets/type/budicons.css')}}" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

<style type="text/css">
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		color:#bbb !important;
	}
	.select2-container--default .select2-selection--single{
		    display: inline-block;
		    -webkit-font-smoothing: antialiased;
		    -webkit-text-size-adjust: 100%;
		    height: 50px;
		    padding: 10px 0px 0px 0px;
		    margin-bottom: 0px;
		    font-size: 36px;
		    font-weight: bold;
		    line-height: 25px;
		    color: #bbb;
		    border: 0;
		    border-bottom: none;
		    background: none;
		    resize: none;
		    vertical-align: middle;
		    -webkit-box-shadow: none;
		    -moz-box-shadow: none;
		    box-shadow: none;
		    -webkit-border-radius: 0;
		    border-radius: 0;
		    -webkit-transition: all 200ms ease-in;
		    -o-transition: all 200ms ease-in;
		    -moz-transition: all 200ms ease-in;
		    width: 100%;
		    text-transform: uppercase;
		    border: 0px;
	}
</style>