<!DOCTYPE html>
<html>
	<head>
		<title>Sudanibit @yield('title')</title>

		@include('front.master.head')


	</head>
	<body class="body-wrapper">
		<div id="particles-js"></div>

		@include('front.master.header')


		@yield('content')

				
		@include('front.master.footer')

		@section('foot')
			@include('front.master.foot')


		@show


	</body>
</html>