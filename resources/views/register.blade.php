<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{url('user/assets/images/favicon.png')}}">

    <!-- App title -->
    <title>Sudanibit- Register</title>

    <!-- Bootstrap CSS -->
    <link href="{{url('user/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- App CSS -->
    <link href="{{url('user/assets/css/style.css')}}" rel="stylesheet" type="text/css" />

    <!-- Modernizr js -->
    <script src="{{url('user/assets/js/modernizr.min.js')}}"></script>

</head>


<body>

<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="login-logo"><img src="{{url('user/assets/images/sudani-bit.png')}}" /></div>
    <div class="account-bg">
        <div class="card-box mb-0">
            <div class="m-t-10 p-10">
                <div class="row">
                    <div class="col-12 text-center">
                        @if(session('error'))
                            <div class="alert alert-danger">
                                {{session('error')}}
                            </div>
                        @endif
                        <h6 class="text-muted text-uppercase m-b-0 m-t-0">Enter your details below to Register</h6>
                    </div>
                </div>
                <form class="m-t-20" id="identicalForm" action="{{url('register_user')}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="text" required="" placeholder="Username" name="name">

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="email" required="" placeholder="Email" name="email">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            {{--<input class="form-control" type="password" required="" placeholder="Password" name="password">--}}
                            <input class="form-control" name="password" required="required" type="password" id="password" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            {{--<input class="form-control" type="password" required="" placeholder="Confirm Password" name="confirmPassword">--}}
                            <input class="form-control" name="password_confirm" required="required" type="password" id="password_confirm" oninput="check(this)" placeholder="Confirm Password"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox-signup" type="checkbox" required>
                                <label for="checkbox-signup">I accept Terms and Conditions</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row text-center m-t-10">
                        <div class="col-12">
                            <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Register</button>
                        </div>
                    </div>

                    <div class="m-t-20">
                        <div class="text-center">
                            <p>Already have account? <a href="{{url('login')}}" class="m-l-5"><b>Login Here</b> </a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end card-box-->



</div>
<!-- end wrapper page -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{url('user/assets/js/jquery.min.js')}}"></script>
<script src="{{url('user/assets/js/popper.min.js')}}"></script><!-- Tether for Bootstrap -->
<script src="{{url('user/assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('user/assets/js/detect.js')}}"></script>
<script src="{{url('user/assets/js/waves.js')}}"></script>
<script src="{{url('user/assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('user/assets/plugins/switchery/switchery.min.js')}}"></script>

<!-- App js -->
<script src="{{url('user/assets/js/jquery.core.js')}}"></script>
<script src="{{url('user/assets/js/jquery.app.js')}}"></script>
</body>
<script>
    function check(input) {
        if (input.value != document.getElementById('password').value) {
            input.setCustomValidity('Password Must be Matching.');
        } else {
            // input is valid -- reset the error message
            input.setCustomValidity('');
        }
    }
</script>
</html>